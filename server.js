const Docker = require("dockerode");
const ZabbixSender = require("node-zabbix-sender");
const {
  listContainers, reportStatistics
} = require("./index");

const docker = new Docker({ socketPath: "/var/run/docker.sock" });
const sender = new ZabbixSender({ host: process.env.ZABBIX_SERVER });

let inDiscovery = false;
async function discovery() {
  try {
    if (inDiscovery) {
      return;
    }
    inDiscovery = true;
    await listContainers(docker, sender, process.env.ZABBIX_HOST);
  } catch (err) {
    console.error(err);
  } finally {
    inDiscovery = false;
  }
}

let inStats = false;
async function stats() {
  try {
    if (inStats) {
      return;
    }
    inStats = true;
    await reportStatistics(docker, sender, process.env.ZABBIX_HOST);
  } catch (err) {
    console.error(err);
  } finally {
    inStats = false;
  }
}

setInterval(discovery, 3600000); // 1h
setInterval(stats, 60000); // 1m
