# docker-stats-zabbix
Zabbix active agent to report docker container statistics (aka `docker stats`).

## Usage
Install `Template_App_Docker_Active.xml` via Zabbix Administrator's interface then link your docker machine host to the template.

On docker machine create and start the container by running:

```
docker run --rm --privileged \
-e ZABBIX_SERVER=your.zabbix.server \
-e ZABBIX_HOST=your.docker.machine.host \
-v /var/run/docker.sock:/var/run/docker.sock \
babinvn/docker-stats-zabbix
```

Or using `docker-compose`:

```
version: '2'
networks:
  intranet:
    external: true
services:
  docker-stats:
    restart: unless-stopped
    image: babinvn/docker-stats-zabbix
    container_name: docker-stats
    networks:
      - intranet
    environment:
      ZABBIX_SERVER: your.zabbix.server
      ZABBIX_HOST: your.docker.machine.host
    volumes:
      - '/var/run/docker.sock:/var/run/docker.sock'
    privileged: true
```

Hopefully you will find items populated and stats being served at 1-minute intervals to your Zabbix server.
