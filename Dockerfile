FROM node:alpine

RUN apk add --no-cache tini \
  && mkdir /app

ADD docker-entrypoint.sh /
ADD index.js /app/
ADD server.js /app/
ADD package.json /app/

WORKDIR /app
RUN npm install

ENV \
  ZABBIX_SERVER="zabbix.server" \
  ZABBIX_HOST="docker"

ENTRYPOINT [ "/sbin/tini", "--", "/docker-entrypoint.sh" ]

CMD ["npm", "run", "serve"]
