const Docker = require("dockerode");
const ZabbixSender = require("node-zabbix-sender");
const {
  listContainers, reportStatistics
} = require("./index");

const docker = new Docker({ socketPath: "/var/run/docker.sock" });
const sender = new ZabbixSender({ host: process.env.ZABBIX_SERVER });

describe("Discovery Test", () => {
  beforeEach(async () => {
    jest.setTimeout(30000);
  });

  it("List containers", async () => {
    const containers = await listContainers(docker);
    expect(containers).toBeInstanceOf(Array);
  });

  it("Discover containers", async () => {
    const { result } = await listContainers(docker, sender);
    expect(result.response).toBe("success");
    console.debug(result);
  });
});

describe("Statistics Test", () => {
  beforeEach(async () => {
    jest.setTimeout(30000);
  });

  it("Get container statistics", async () => {
    const containerObj = await reportStatistics(docker);
    expect(containerObj).toBeInstanceOf(Object);
  });

  it("Report container statistics", async () => {
    const { result } = await reportStatistics(docker, sender);
    expect(result.response).toBe("success");
    console.debug(result);
  });
});
