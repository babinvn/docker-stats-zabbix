const { hostname } = require("os");

// Report all containers for Zabbix LLD
async function listContainers(docker, sender, hostName) {
  return new Promise(async (resolve, reject) => {
    try {
      if (!docker) {
        reject(new Error("Invalid docker argument, should be a valid dockerode docker instance"));
        return;
      }

      const containers = await docker.listContainers({ all: true });
      const containerNames = [];
      containers
      .filter(container => container.Names && container.Names.length > 0)
      .forEach(container => containerNames.push(container.Names[0].substr(1)));

      if (!sender) {
        resolve(containerNames);
        return;
      }

      const data = [];
      containerNames.forEach(containerName => data.push({ "{#CONTAINER}": containerName }));
      sender.addItem(hostName || hostname(), "container.discovery", JSON.stringify({ data }));
      sender.send(function(err, result) {
        if (err) {
          reject(err);
        } else {
          resolve({ result });
        }
      });
    } catch(err) {
      reject(err);
    }
  });
}

// Report statistics for running containers
async function reportStatistics(docker, sender, hostName) {
  const getStats = async (docker, containerId) =>
    new Promise(async (resolve, reject) => {
      try {
        const container = await docker.getContainer(containerId);
        const stats = await container.stats({ stream: false });
        resolve(stats);
      } catch(err) {
        reject(err);
      }
    });

  function calculateStats(stats) {
    const calc = {
      cpuPercent:
        (stats.cpu_stats.cpu_usage.total_usage - stats.precpu_stats.cpu_usage.total_usage)
        / (stats.cpu_stats.system_cpu_usage - stats.precpu_stats.system_cpu_usage)
        * 100,
      pids: stats.pids_stats.current,
      mem: stats.memory_stats.usage,
      memLimit: stats.memory_stats.limit,
      memPercent: stats.memory_stats.usage / stats.memory_stats.limit * 100,
      blkRead: 0,
      blkWrite: 0,
      netRx: 0,
      netTx: 0
    };
    Object.values(stats.networks).forEach(net => {
      calc.netRx += net.rx_bytes;
      calc.netTx += net.tx_bytes;
    });
    stats.blkio_stats.io_service_bytes_recursive.forEach(bioEntry => {
      if (!bioEntry.op) {
        return;
      }
      switch (bioEntry.op.toLowerCase()) {
        case "read":
    			calc.blkRead += bioEntry.value;
          break;
    		case "write":
    			calc.blkWrite += bioEntry.value;
      }
    });
    return calc;
  }

  return new Promise(async (resolve, reject) => {
    try {
      if (!docker) {
        reject(new Error("Invalid docker argument, should be a valid dockerode docker instance"));
        return;
      }

      const containers = await docker.listContainers({ all: true });
      //console.debug("Containers", containers);
      const containerObj = {};
      containers
      .filter(container => container.Names && container.Names.length > 0 && container.State === "running")
      .forEach(container => {
        containerObj[container.Id] = { name: container.Names[0].substr(1) };
      });

      const containerIds = Object.keys(containerObj);
      const stats = [];
      containerIds.forEach(containerId => stats.push(getStats(docker, containerId)));
      const statsData = await Promise.all(stats);

      containerIds.forEach((containerId, index) => {
        //console.debug(statsData[index]);
        //console.debug(containerObj[containerId].calc);
        containerObj[containerId].stats = calculateStats(statsData[index]);
      });

      if (!sender) {
        resolve(containerObj);
        return;
      }

      containers
      .filter(container => container.Names && container.Names.length > 0 && container.State !== "running")
      .forEach(container => {
        sender.addItem(hostName || hostname(), `container.up[${container.Names[0].substr(1)}]`, 0); // not running
      });

      Object.values(containerObj).forEach(container => {
        sender.addItem(hostName || hostname(), `container.up[${container.name}]`, 1); // running
        sender.addItem(hostName || hostname(), `container.cpuPercent[${container.name}]`, container.stats.cpuPercent);
        sender.addItem(hostName || hostname(), `container.pids[${container.name}]`, container.stats.pids);
        sender.addItem(hostName || hostname(), `container.mem[${container.name}]`, container.stats.mem);
        sender.addItem(hostName || hostname(), `container.memLimit[${container.name}]`, container.stats.memLimit);
        sender.addItem(hostName || hostname(), `container.memPercent[${container.name}]`, container.stats.memPercent);
        sender.addItem(hostName || hostname(), `container.blkRead[${container.name}]`, container.stats.blkRead);
        sender.addItem(hostName || hostname(), `container.blkWrite[${container.name}]`, container.stats.blkWrite);
        sender.addItem(hostName || hostname(), `container.netRx[${container.name}]`, container.stats.netRx);
        sender.addItem(hostName || hostname(), `container.netTx[${container.name}]`, container.stats.netTx);
      });
      sender.send(function(err, result) {
        if (err) {
          reject(err);
        } else {
          resolve({ result });
        }
      });
    } catch(err) {
      reject(err);
    }
  });
}

module.exports = {
  listContainers,
  reportStatistics
}
